<%--
  Created by IntelliJ IDEA.
  User: alex
  Date: 07.07.19
  Time: 11:11
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <title>Add Client Form</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

</head>
<body>

<div class="w3-container w3-center w3-green" align="center">
Add Client Form
<br/>

<form action="clients" method="post">
    First Name: <input type="text" name="first_name"/><br/>
    Last Name: <input type="text" name="last_name"/><br/>
    <br/>
    <input type="submit" value="Add Client"/>
</form>

<form name="return" action="home.jsp" method='post'>
    <input type='submit' value='Return Home'/>
</form>
</div>
</body>
</html>
