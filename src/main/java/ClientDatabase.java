import java.util.Set;
import java.util.TreeSet;

public class ClientDatabase{
    private static ClientDatabase instance = new ClientDatabase();
    protected static Set<Client> clientsList;

    public static ClientDatabase getInstance(){
        return instance;
    }

    private ClientDatabase(){
        clientsList = new TreeSet<Client>();
    }

    public static boolean addClientToDatabase(Client client){
        if (clientsList.add(client)){
            return true;
        } else {
            return false;
        }
    }
}
