import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@WebServlet(urlPatterns = { "/clients_table" })
public class ClientsTable extends HttpServlet {


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        Set<Client> clientsTable = ClientDatabase.clientsList;
        request.setAttribute("lik",clientsTable);
        request.getRequestDispatcher("clients_table.jsp").forward(request,response);
    }
}
