public class ClientAlreadyExistsException extends Exception {
    public ClientAlreadyExistsException(){
        super("Client already exist in database");
    }
}

