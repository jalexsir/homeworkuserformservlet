import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = {"/clients"})
public class AddClientServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String first_name = request.getParameter("first_name");
        String last_name = request.getParameter("last_name");

        ClientDatabase database = ClientDatabase.getInstance();

        try {
            if (database.addClientToDatabase(new Client(first_name, last_name))) {
                request.getRequestDispatcher("/printmessage.jsp").forward(request, response);
            } else {
                throw new ClientAlreadyExistsException();

            }
        } catch (ClientAlreadyExistsException e){
            request.getRequestDispatcher("/errormessage.jsp").forward(request, response);
            e.printStackTrace();
        }

    }

}
