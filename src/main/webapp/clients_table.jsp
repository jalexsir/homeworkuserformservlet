<%@ page import="java.util.TreeSet" %><%--

  Created by IntelliJ IDEA.
  User: alex
  Date: 07.07.19
  Time: 10:46
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Clients List</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


</head>
<body>

<table class="w3-container w3-center w3-green" align="center">
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
    </tr>

    <c:forEach items="${lik}" var="client">
        <tr>
            <td>${client.first_name}</td>
            <td>${client.last_name}</td>
        </tr>
    </c:forEach>

</table>

<form  class="w3-container w3-center w3-green" align="center" name="return" action="home.jsp" method='post'>
    <input type='submit' value='Return Menu'/>
</form>
</body>
</html>